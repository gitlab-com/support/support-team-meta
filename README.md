# Support Team - Project

Welcome to the GitLab Support team project!

We use this space as a team issue tracker, check out our [open issues](https://gitlab.com/gitlab-com/support/support-team-meta/issues) or create a [new issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/new).

Support team resources:

* Handbook: https://handbook.gitlab.com/handbook/support/
* Support Workflows: https://handbook.gitlab.com/handbook/support/workflows/
* Working with Support Ops - Propose a change: https://handbook.gitlab.com/handbook/support/workflows/working_with_support_ops/#propose-a-change
* GitLab docs: http://docs.gitlab.com/
* Team timezones: https://timezone.io/team/gitlab-support

## Issues triage

 We use the Gitlab Bot integration from the [gitlab-triage](https://gitlab.com/gitlab-org/gitlab-triage)
 project to automate issue triaging, see the rules in [.triage-policies.yml](./.triage-policies.yml)
 for more details.

 Note: if changing the triage policies, dry-run operations don't work on non-protected branches,
 the workaround for now is to merge and trigger a dry-run operation on master, then revert if it
 fails (as the actual runs only happen in scheduled jobs).
