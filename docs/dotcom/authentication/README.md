## Authentication Procedures and Policies

#### At the time of this writing, we are still working on setting an authentication method and policy. Please see [our internal issue](#1352) for discussion regarding this. In the meantime, please follow the whatever guidelines we have when answering customer tickets.

### Guides

- [Do's and Dont's](./do_dont.md)
