<!--

Use this template when you have a request for a new dashboard in Tableau, Zendesk or need some adhoc data pulled

1. Give your issue a good title
1. Complete all sections below
1. Set a due date

From there Readiness will:
1. Triage the request, asking any relevant questions to make sure we're building the right thing.
1. Estimate the time to complete the request.
1. Schedule the request and make a related Support Ops issue for action.
-->
# Data Request

## Purpose

<!-- 
Describe the data you're looking for. What are you trying to convey by gathering this data? Who is the audience?
-->

- [ ] This is SAFE data

### Platform

- [ ] Zendesk Explore
- [ ] Tableau
- [ ] Data export into a spreadsheet
- [ ] Other: _specify_

### Report lifetime

 - [ ] **Evergreen** - we'll use this data forever and you need to make sure it stays accurate.
 - [ ] **Adhoc** - I care about this now, but if the report gets deleted next month it's okay.
 - [ ] **Something in between** - I need this data to monitor a change, but after the monitoring period is complete we don't need it any more.
    - Specify time period: `YYYY-MM-DD` to `YYYY-MM-DD`

### Data Time Frame

<!--
  - For the last two years? Six months? What time slice of data are you interested in?
-->

### Issue(s) Linked to Data Request 

<!--
List any related issues or merge requests that are connected to this data request. This helps provide context and background information.
-->

- [ ] Issue #
- [ ] Merge Request #


## Approach

<!--
Describe your idea to get that data - where is it located, does it exist at all? How would you approach this problem?
-->

/label ~"FY25 OKR::Process : Enhance our systems" ~"Readiness::Triage" ~"Readiness Specialization - Data Analysis & Services Delivery"
/assign @Melissa_Magoma @dtragjasi @nabeel.bilgrami 
