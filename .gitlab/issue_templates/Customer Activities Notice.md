## Situation

**Template Instructions - Remove**

Describe here the situation that is creating the special need. For example:
> ACME Systems is conducting their first repository backup this weekend in preparation for the migration from SVN to GitLab next weekend of approximately 1 bazillion users. They are concerned that they may encounter issues with the backup, and have requested weekend help from GitLab Support. As discussed in Slack, Director 1 and Manager A have granted this request. We have advised ACME to include the following text in any email message that they send to initiate an emergency request:
> 
> ACME - Special Emergency

Include details such as the following, if applicable:
- Date and time range of planned activity (use UTC time)
- Supplementary support available (for example a named support engineer briefed on the situation and available to support the on-call responder)
- Scope agreements on what we can and can't help with
- Relevant Slack channels specific to this activity if they exist

**End Template Instructions**

## Issue Author Action Plan

1. [ ] Add an event to the [Support - Customer Events](https://calendar.google.com/calendar/embed?src=c_8d5a8e9b8c3fc74901bad1799b18e8eafc9e499f7805f9c82f79f9d1e1f9ac4b%40group.calendar.google.com)
   1. [ ] Link back to this issue from the calendar event
1. [ ] Update the checklist below to contain the correct on-call personnel for the impacted dates
1. [ ] Update the [On-Call Team Guidance](#on-call-team-guidance) section below with any instructions you have that are specific to this situation

## On-Call Team Guidance

1. Please click the checkbox next to your name, below, to let us know that you've seen and read this issue
1. If the customer submits an emergency ticket, once you have finished working on it, please submit a comment in this issue:
   1. summarizing the problem and all actions taken,
   1. linking to the emergency ticket

   The purpose of the comments is to provide helpful data to anyone who might receive a later emergency request from the customer on the same weekend.
1. *Enter issue-specific instructions here*

## Yup, I read it

### APAC

* [ ] `on-call SE 1 handle`
* [ ] `on-call SE 2 handle`
* [ ] `on-call manager handle`

### EMEA

* [ ] `on-call SE 1 handle`
* [ ] `on-call manager handle` 

### AMER

* [ ] `on-call SE 1 handle`
* [ ] `on-call SE 2 handle`
* [ ] `on-call SE 3 handle`
* [ ] `on-call manager handle`

## Reference
Please see the Handbook page TBD


<!-- Attach the "not applicable" label for the current fiscal year, such as:
    /label ~"FY26 OKR::Not Applicable"
-->

/label ~"Support:Customer Activity"
/confidential
