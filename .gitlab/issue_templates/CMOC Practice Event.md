# CMOC Practice event

- [ ] Set title to `CMOC Practice event FY<YY>-Q<N>` - for example - `CMOC Practice Event FY23-Q1`
  - See [Fiscal Year](https://about.gitlab.com/handbook/finance/#fiscal-year) handbook page for help with the dates
- [ ] Agree with the CMOC on the script to use (can be changed for each event depending on experience)
- [ ] Assign the issue to all people that will be participating in the event
- [ ] Before paging, double-check the [PagerDuty shadow rotations](https://gitlab.pagerduty.com/schedules/P1UHNJP). If there is anyone shadowing at the time, inform them that they can ignore this practice incident, or they can join the practice incident to gain more experience.
- [ ] Add an override into the shadow CMOC schedule, for the person in the CMOC role in the practice incident.
  - **Tip**: The [PagerDuty escalation policy](https://gitlab.pagerduty.com/escalation_policies/#PNH1Z1L) is setup so that anyone "oncall"  at the time of the "incident" will get paged. So additional people can be paged for this practice event by adding overrides into another region's shadow rotations as well.  

## References

- [Script Library](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/Support%20Specific%20Trainings/cmoc-practice-events-scenarios) - confidential to IM and note taker
- [CMOC Practice Events](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#cmoc-training-activities) process details

## Event Runbook

The practice event requires at least 2 people: a person to act as the **IM**, and a second person to act as **CMOC**. After both people agree to start, please follow the steps below to create a practice event.

Please make sure **not** to start the practice event if we already have a production incident in progress. To check for production incidents see `#incident-management` as well as `#production` slack channels and the [GitLab.com Status Page](https://status.gitlab.com/).

## Initiate Practice Incident

1. [ ] **IM** Notify `@sre-oncall` and `@cmoc` in the [#production](https://gitlab.slack.com/archives/C101F3796) Slack channel that a CMOC practice event is taking place; post a link to this issue in your message.
   1. Feel free to change the following message for the SRE team:

   ```text
   Hello @sre-oncall and @cmoc, the Support team will start a [CMOC Practice event](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/#cmoc-training-activities) in a few minutes.
   Please note that your attention for the "TEST: Shadow CMOC Practice Event" is **not** required.
   In case you have any questions or concerns please feel free to ask on the [CMOC Practice Event issue](link-to-this-CMOC-Practice-Event-Issue) or by mentioning the assigned people in this Slack thread, thank you. :)
   ```

1. [ ] **IM**: [initiate](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#report-an-incident-via-slack) the practice incident by using `/incident declare` in [#production](https://gitlab.slack.com/archives/C101F3796)

- set severity as `S4`
- set title as `TEST: Shadow CMOC Practice Event`
- **ONLY** page the `communications manager on-call` (which will page the shadow CMOC as well)

The following will be done by the incident app:

- Practicing CMOC paged via shadow rotation
- Incident auto-created for the event
- Slack channel auto-created for the event - use this for any discussion during the practice event

## Introduction to event

1. [ ] **IM** Notify the CMOC trainee that we are going to use the same zoom meeting.
1. [ ] **IM** Once the incident is created **remove all** labels from the incident issue so it doesn't impact incident stats.

## Practice Event Begins

1. [ ] **IM** starts recording on Zoom and follows the script for the chosen scenario
1. [ ] **CMOC** logs into status.io, using the *test page* and posts updates as appropriate to the scenario
   - Remember to refer to the [CMOC Workflow page](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows/)
1. [ ] **IM** to paste status.io test page updates to incident slack channel for reference

## Discussion points

1. Was this helpful?
1. Was the documentation clear?

## Post practice event actions

**CMOC** and **IM** to agree on DRI for the following:

1. Note any improvements/corrections/feedback in the issue
1. Update template documents and handbook as necessary

/label ~"FY26 OKR::Not Applicable"
