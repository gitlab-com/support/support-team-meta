TITLE: Resulting changes from the Deep Dive on TOPIC

As a result of the [deep dive on TOPIC](issue-link) the [following artifacts](https://handbook.gitlab.com/handbook/support/advanced-topics/#artifacts) have to be created:

- [ ] Example (Mandatory if a relevant bootcamp exists): As the last step before answering tickets, link to the deep dive recording in the XXXX bootcamp
- [ ] Example: Clarify XXXX on [this](link-to-docs-page) page in the docs
- [ ] Example: Create issue to add XXXX feature to improve the customer experience
- [ ] Example: Add a section on XXXX to the bootcamp
- [ ] Example: Add troubleshooting steps for when XXXX happens

You may find the following links helpful:

- [Google doc](link-to-deep-dive-google-doc)
- [Deep Dive recording](link-to-recorded-deep-dive)

If you are assigned to this issue, please pick at least one task from the above list (pairing is encouraged) and, after creating the merge request or issue, add it to the [deep dive issue](deep-dive-issue-link#results)'s description, under the **Results** heading. Please ask for help where you need it.

/assign `@all-participants @assistant @host`
/label ~Documentation ~Meta ~Doing
/due in 1 month
/label ~"FY26 OKR::Not Applicable"
