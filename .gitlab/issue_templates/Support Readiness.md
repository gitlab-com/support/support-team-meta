<!--
Title: 15.x.x Support Readiness - Brief Description
Replace release number with date 20YY-MM-DD if more applicable

If Security related and created prior to release, make /confidential
-->

<!-- Set appropriate date based on release date -->
/due 1 week

## What is happening
<!--
Give a 1-3 sentence executive overview of what situation is occuring
that requires Support to be prepared.
-->

### Status / What actions have been taken so far
<!-- Brief summary -->

### Timeline / Important Dates

### Related Issues/MRs/Epics
<!-- Remove any that are not applicable -->

- Issue/epic: 
- MR: 
- Feature flag issue: 
- Feedback issue: 
- Docs: 
- Blog post: 

## What impact will this have on users?
<!-- Brief summary -->

### What this may look like for Support

Anticipated Support Impact: <!-- Low/Medium/High/Unknown -->

<!-- If there is communication already created that would cover below, you can link to that instead. -->

What errors or messages users may report: <!-- how can support identify these tickets? -->

What workarounds/solutions are available?: <!-- feel free to link to an issue comment or similar -->

### Do users need to be contacted?

- Yes/No
- If Yes, please link to the [marketing ops issue](https://handbook.gitlab.com/handbook/support/workflows/sending_notices/#mass-emails-through-marketing-department): 

## DRIs/Contacts for questions and approvals for communications/action items

- Slack Channel: 

- Product or Development DRI: 
- Security DRI (if applicable): 
- Support DRI: 
<!-- If you are unsure and not already in contact with a Support team member, you can use: @gitlab-com/support/readiness -->
- Support Manager DRI (if needed): <!-- Required if Support DRI is not manager. If unsure, leave for Support DRI to nominate -->

--- 
## Support Resources
<!-- 
This section will be used after triage to list any additional details, such
as ZD Macros. If you're outside of Support, feel free to add items below
but if you don't know or they don't exist yet - that's okay! 
-->

- FAQ for Support: 
- Other resource: 

### Zendesk Macros
<!-- Remove if not relevant. A macro is typically not necessary if users are being contacted directly. -->

Zendesk tag: ``

1. [ ] Macro MR: 
    1. [ ] Macro adds appropriate tag
    1. [ ] Set DRIs as reviewers

### Communication to Support team

1. Announced to team in
    1. [ ] `#support_gitlab-com` or `#support_self-managed` or `#support_team-chat`
    1. [ ] Support Week in Review[(SWIR)](https://gitlab.com/gitlab-com/support/readiness/support-week-in-review/-/issues/?sort=created_date&state=all&first_page_size=10). Use [this link](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/) add it to the SWIR in the `Things to know about` category.

---

/label ~"Support Readiness" ~"OKR FY24::Customer Results"
