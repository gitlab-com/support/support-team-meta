<!--
Issue Title: "PD Schedule change: <schedulename> <add/remove/change> <person/timing/???>"  If multiple schedules are impacted, briefly describe the scope of schedules impacted (i.e. Global CMOC, all APAC schedules)
Example: "PD Schedule change: CMOC (APAC Group 1) add Firstname Lastname" or "PD Schedule change: CMOC EMEA change ordering"
-->

**PURPOSE:** This template can be used to create a record of a manager executed change, to a schedule affecting team members in their own region. If the change 
- has a wider scope, (ie cross-region),
- or is for creating/deleting schedules or modifying schedule configurations (not just participant changes),
- or if you would like Suppport-Ops to execute the changes, 

please create an issue using the templates in the [Support-Ops PagerDuty project](https://gitlab.com/gitlab-com/support/support-ops/pagerduty/-/tree/master/.gitlab/issue_templates). 

--- 

## List the schedule being changed (include link to PD)
<!--
Where-ever possible, create a different issue for each schedule being changed.  Adding/removing multiple people in a single schedule in one change is fine.
-->
- [schedulename](PDlink)

## What is changing
<!--
NOTE: It can be preferable to create a new layer when making changes - this allows those impacted to view the starting start compared to the end state, which can be particularly useful for checking that any existing overrides are still relevant.
Describe the specific changes in bullet points - for example:
"- Create a new layer to:
   -add @person  to rotation between person abc & def"
"- remove @person from rotation"
"- change rotation from 5 days to 7 days"
Consider describing current state and resulting state if ordering of participants is effected.
-->
- 

## What date will the change take effect?
<!--
PD changes be future dated.  Note the date that the change will take effect (this is NOT the date of implementation - ie you could make the change to the schedule in May, to remove a person from the rotation effective in August - note here the August date)
-->
- 

## Are there any existing overrides?
<!-- if yes, please list them -->


## Task list
- [ ] Discuss with effected people if needed (i.e. rotations with less than 6 people or short-notice changes)
- [ ] Implement change or new layer
- [ ] if using a layer, set the due date on this issue to 3-4 weeks after the change has taken effect
   - [ ] at due date remove the original layer

/label ~"FY26 OKR::Not Applicable" 
