Title: Deep Dive on `TOPIC`

# Deep Dive on `TOPIC`

- Date:
- Host(s):
- Participant Cap: `<cap>`
- Participants:
  - `Participant 1`
  - `Participant 2`
  - `Participant 3`
- Assistant:
  - `Participant-volunteer`
- Complements the following training:
  - `links to relevant documentation and/or training module`

## Description

`<High Level Description of what the focus of this deep-dive will be>`

### Objectives

After the deep dive each participant will be able to:

- `Objective 1`
- `Objective 2`
- `Objective 3`

Please also see the [documented goals of a Support Deep Dive](https://handbook.gitlab.com/handbook/support/advanced-topics/#goals).

---

## Tasks

### Host

#### Before the call

- [ ] Host: Please read about your [role and responsibilities as a host](https://handbook.gitlab.com/handbook/support/advanced-topics/#the-host).
- [ ] Host: Find, at minimum, 3 participants. You can find them by either asking the Support team to see who's interested or even [looking for who's taking a related training module](https://gitlab.com/gitlab-com/support/support-training/issues).
- [ ] Host: Of the participants, find one to be the [assistant](https://handbook.gitlab.com/handbook/support/advanced-topics/#assistant). If you haven't already done so, assign them to this issue alongside yourself.
- [ ] Host: Think about what prerequisites participants will have to complete so to best be ready for a Deep Dive (i.e. read through specific documentation, set up and use this feature with GitLab, finish the first 2 stages of a training module, running through a few planned scenarios, etc.). Add these to the **Before the call** section for participants.
- [ ] Host: Announce this deep dive to the Support team using in the `#support-team-chat` Slack channel and the [Support Week in Review](https://handbook.gitlab.com/handbook/support/#support-week-in-review).
- [ ] Host: Create a **public** Slack channel and, as per our [Internal Communication docs](https://handbook.gitlab.com/handbook/support/#spt_vs-support_-prefix), name it `spt-TOPIC`. Invite all participants and use this channel to communicate with them and so they can help each other prepare. Link to this issue in the [channel's topic](https://get.slack.help/hc/en-us/articles/201654083-Set-a-channel-topic-or-purpose).
- [ ] Host: Create a [Google Doc](https://about.gitlab.com/handbook/communication/#google-docs) and ask participants to add to it any questions they have or anything else they'd like to discuss on the call. You may also find this doc helpful when planning how to structure the call. If any questions were added you don't know the answer to, do your best to find the answer. If you're using any Slides or any other docs for the call, link them in this doc too. Add the following disclaimer to the top of the doc:
    >This is a working doc for real time collaboration.
    >
    >We're targeting to deprecate it and have the content moved to our product documentation after the Deep Dive
- [ ] Host: Create a plan for what to discuss, troubleshoot, or just talk about during the Deep Dive. Stay aware of the [objectives](#objectives) you've listed above. Be sure to set aside some time to answer the questions from the Google doc. If there are any slides or additional notes you'll be using, share them here and in the Google doc.
- [ ] Host: Once the participants have finished their preparations, [schedule the call](https://about.gitlab.com/handbook/communication/knowledge-sharing/index.html#schedule-the-call) at a time that works best for all involved. Invite all the participants and `GitLab Support`; include links to the Google doc, the issue you created, the slides (if any exist), and the Slack channel in the description. Make sure the call gets recorded.

#### During the call

- [ ] Host: Confirm that the call is being recorded.
- [ ] Host: Mention the latest version of GitLab so the recording will have that context.

#### After the call

- Host: Recording
  - [ ] Upload to YouTube (set to private if necessary) mentioning "Support" in the title
  - [ ] Add it to the [Deep Dive playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqsiWzD0YZYp9xSxgTRoWxH)
  - [ ] Optionally, upload to [Support GDrive Training/Deep Dive folder](https://drive.google.com/drive/folders/1Yli_1UlO1vrzhCuNN1pdZLRpqbfYQW7Q)
- Host: Share a link to the recording with:
  - [ ] The Support team in the [Support Week in Review](https://handbook.gitlab.com/handbook/support/#support-week-in-review)
  - [ ] The Engineering team in the [Engineering Week in Review](https://about.gitlab.com/handbook/engineering/#communication)
  - [ ] Add it to this issue, under **Recording**
- [ ] Host: Help Participants with any questions they have or help they need with the issues and/or merge requests they have to create.
- [ ] Host: After the last issue/merge request is completed, close this issue, close the issue created for the after-call tasks, and archive the Slack channel.

---

### Participants

#### Before the call

- Please read about the [role and responsibilities of a participant](https://handbook.gitlab.com/handbook/support/advanced-topics/#participants).
- Complete the following tasks to understand the basics about the subject:
  - `Pre-learning 1`
  - `Pre-learning 2`
  - `Pre-learning 3`

As you go through the above tasks, discuss your experience with the others in the Slack channel. Please ask for help where you feel you need it and provide it where you can. Feel free to pair on these tasks too. Add any questions you have or any unclear documentation you find to this deep dive's Google doc.

#### After the call

- You will be assigned to an issue to help [create artifacts](https://handbook.gitlab.com/handbook/support/advanced-topics/#artifacts). Please pick one or more tasks to complete and do so. Pairing is encouraged. Ask the host, other participants, or even others at GitLab for any help you need.

---

### Assistant

#### Before the call

- [ ] Assistant: Please read about your [role and responsibilities as an assistant](https://handbook.gitlab.com/handbook/support/advanced-topics/#assistant).
- Complete your tasks as a Participant.

#### During the call

- [ ] Assistant: Take detailed notes during the call. Include any lessons, questions, answers, troubleshooting steps, suggestions for improvements, etc.

#### After the call

- [ ] Assistant: Using the notes from the Deep Dive call, create a list of tasks to make any external improvements. Aim to move everything from the Google docs to a permanent location.
- [ ] Assistant: [Create a new issue](https://gitlab.com/gitlab-com/support/support-team-meta/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to create the artifacts of this call using the **Deep Dive (post-call)** template. Fill it out completely. Read up on [deep dive artifacts](https://handbook.gitlab.com/handbook/support/advanced-topics/#artifacts) to get an idea of the kind of what kind of tasks to create. Consult with the host if you'd like some help. Issue link: `issue-link`
- [ ] Assistant: In this Deep Dive's Slack channel, inform everyone of the above issue and that they should pick tasks to work on.
- [ ] Assistant: After all tasks are completed, confirm that all relevant content from the Google doc made it's way into the docs. If it did, then [deprecate it](https://about.gitlab.com/handbook/communication/#how-to-deprecate-a-google-doc).
- [ ] Assistant: Add any issues or merge requests that are created to the **Results** section, below.

---

## Results

In addition to team-level enablement, [deep dives should result in external facing improvements](https://handbook.gitlab.com/handbook/support/advanced-topics/#artifacts). Please document the outcomes of the deep-dive.

### Recording

`<Drive Link>`

### Merge Requests

- `<link>`
- `<link>`

### Issues

- `<link>`
- `<link>`

/due `<set date here>`
/label ~Doing ~"Team Training"
/assign `@the-host` `@the-assistant`
/epic &109
/label ~"FY26 OKR::Not Applicable"
