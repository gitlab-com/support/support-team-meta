<!--
Title: Support 18.0 Readiness - Removal/Deprecation - Feature name

Typical example format, change as necessary

Note: This is different from a general Support Readiness issue as it is tied to feature change in a specific release.
-->

# Summary

The purpose of this issue is to:

- Assess the impact of the following planned feature removal or deprecation planned.
- Document what actions customers must/should perform to mitigate the impact of the change.
- Track findings from support tickets and other sources related to the change.

**Links to Change:** 

- release notes/deprecation notice
- e.g. EPIC, ISSUES, DOCS

Product or Development DRI:
- **Product or Development DRI**: `@username`
- **Support DRI**: `@username`

# Impact Assessment

[MANDATORY]

**Mitigations/Workarounds**

[OPTIONAL]

**Support Notes**

[OPTIONAL]

# Action items

/epic &330
<!--
Epic: Support Readiness - GitLab 18.0

Please update the epic as needed. For a new major release, update the template file.
-->

/label ~"FY26 OKR::Not Applicable" 
/label ~"Support Readiness" 