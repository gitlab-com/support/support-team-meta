<!--
Title: Brief objective description of proposed change.
-->

# Problem Statement

<!--

This where you should detail your problem statement. In doing so, you want to
ensure you cover both WHAT the problem statement is and WHY it is a problem.

Example Problem Statement:

## What is the problem?

Zendesk Tickets using the gitlab.com form and mention they are a Sales Assisted
Trial do not get the prospect tag.

## Why is this a problem?

This causes improper routing, places the tickets in the wrong views, and does
not apply the correct SLA to the tickets.

-->

## What is the problem?



## Why is this a problem?



# Proposal

<!--

This is where your proposal to fix the problem statement should go. It should
follow the philosophy of 
[MVC](https://about.gitlab.com/handbook/values/#minimum-viable-change-mvc). If
required, mention potential iterations that can follow at a future time.

Example Proposal:

To fix the problem statement, I propose we adjust the trigger that applies the
Prospect tag to tickets using the Self-Managed form to also apply it to tickets
using the Gitlab.com form.

As a future iteration, perhaps we should check the overall routing process to
ensure all prospects are getting the label. If the form Gitlab.com was missed,
it is possible other routing avenues were missed as well?

-->



# DRI

<!--

This should contain who will act as the DRI for this issue. Make sure to assign
this issue to whomever is acting as the DRI, as well as anyone else that needs
this assigned to them.

Example DRI:

@jcolyer will act as the DRI for this issue.

-->

PERSON will act as the DRI for this issue.

/assign PERSON


# Required Resources

<!--

This should detail what resources might be required for the proposal. This can
include talking to other departments, the use of GKE, etc. If this requires an
access request to get said resources, ensure the proper channels are utilized to
facilitate that.

Example Required Resources:

* This will require access to the Zendesk Sandbox to test the changes. As I do
  not currently have access to this, I will need to submit an Access Request.
  cc @my-manager 
* As this is simply a change to the handbook, no additional resources would be
  required.
* I will need to pair with support-ops to accomplish this. cc @gitlab-com/support/support-ops 

-->



# Potential Roadblocks/Things to consider

<!--

This is an area for any potential roadblocks you might encounter during the work
from this issue or anything you think should be considered/talked about. This
might not always have data, as sometimes you may not know about roadblocks until
they have been discussed (you can always update it later to include information
that has been surfaced).

Example Potential Roadblocks/Things to consider

* As this is a pretty big change to our upgrade process, we would need to speak
  with the Sales team first to ensure this change is viable.
* I am not sure if this is entirely possible with the
  [view limits](#link-to-docs) Zendesk currently has
* This would require buy-in from @jscarborough before proceeding.
* N/A at this time

-->



# Desired Outcome

<!--

This should include the desired outcomes of this issue. It should be able to
answer the following questions:

* What does success look like?
* How do we measure success?
* Where would future feedback go?

Example Desired Outcome:

* ## What does success look like?

Once this has been solved, we should be able to verify that the views handling
tickets using the GitLab.com form and marked as Sales Assist Trial have the
prospect tag.

* ## How do we measure success?

This should result in a higher volume of tickets in these views, which we could
verify via Zendesk Explore.

* ## Where would future feedback go?

Future feedback regarding this issue should likely have a new issue created
(with this one linked to them) so it can be discussed and rectified via
support-ops.

-->

## What does success look like?



## How do we measure success?



## Where would future feedback go?



# Related Issues/MRs/Epics/Tickets

<!--

This should contain any related issues, merge requests, epics, or Zendesk
tickets. When linking GitLab issues, merge requests, and epics, this will ensure
they are auto-linked via GitLab.

When linking Zendesk tickets, please ensure this issue gets linked to the
Zendesk ticket itself as well!

As a note, you can also link to the epic during the creation of this issue via
the GitLab form.

Example Related Issues/MRs/Epics/Tickets

* https://gitlab.zendesk.com/agent/tickets/1
* https://gitlab.com/gitlab-com/support/support-team-meta/issues/1
* &1

-->



---

/label ~"FY26 OKR::Not Applicable" 
