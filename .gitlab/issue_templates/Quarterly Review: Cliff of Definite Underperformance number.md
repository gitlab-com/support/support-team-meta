
<!-- Title: Notification Issue: APAC - Cliff of Definite Underperformance number 
reviewed for end of FY<INSERT yy>-Q<INSERT quarter>
eg. Notification Issue: APAC - Cliff of Definite Underperformance number 
reviewed for end of FY24-Q4
-->

## In short

- The Cliff of Definite Underperformance was reviewed for the 12 month period concluding at 
the end of FY<INSERT yy>-Q<INSERT quarter> (<INSERT Date last day of review period quarter>).
- The number has <INSERT :arrow_up: increased | :arrow_down: decreased>, to **<INSERT new 
number>**. 
<!-- Create an MR to update the number and historical data tables
https://handbook.gitlab.com/handbook/support/apac/#cliff-of-definite-underperformance 
and link below
-->
- The handbook has been [updated](URL) 
with the post-review number, and to add a row to the historical data table.

### Background

The Cliff of Definite Underperformance number is reviewed each quarter, as described under 
`Frequency` in the Cliff of Definite Underperformance [handbook section](https://gitlab.com/gitlab-com/content-sites/handbook/blob/main/content/handbook/support/apac.md#cliff-of-definite-underperformance). 

This was previously reviewed for the 12 month period ending <INSERT Date last day of quarter 
PRIOR to review period quarter>. At that time, the number was observed to be <INSERT previous 
number>.

This issue serves as 
- a record of the review carried out for the 12 month period ending <INSERT Date last day of 
review period quarter>
- a notification to `@gitlab-com/support/apac` that the number has been reviewed

This issue will be closed in 1 week.  Feedback or questions are welcomed here, including after 
the issue is closed.

/assign me
/label ~"FY26 OKR::Not Applicable" 
