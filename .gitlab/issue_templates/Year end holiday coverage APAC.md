<!--
Title: APAC Holiday Coverage YYYY - dd December YYYY to dd January YYYY
Replace YYYY with current year and dates from on or around 
25 December (start on the Monday) and end date the Friday after 
01 January of the next year.
-->

<!-- Set appropriate date 2-weeks prior to the Monday start date -->
/due 1 week

# Purpose of this issue
<!-- Purpose - update any links to views or changes to process since
the previous year, that needs to be captured here
 -->

This issue guides the APAC support team on coverage for the end of year holiday period.  

- We will not have people present from every Support Global Group during this period. 
- We have defined three roles that will ensure that we handle incoming tickets as a full team effort. 
   1. Eyes On FRTs - 2 people per day to cover all APAC hours
   2. NRT triage - a manager will do this
   3. Working on tickets - all Support Engineers present
- 2 views have been made available to everyone that can be used for this period, and these includes tickets from all SGGs, and from L&R
   1. `Global FRTs` [Zendesk view](https://gitlab.zendesk.com/agent/filters/7818750574108)
   2. `All NRT` [Zendesk view](https://gitlab.zendesk.com/agent/filters/360075980520)
- Everyone can work from these views for picking up tickets, with the priorities as defined below. It's likely that ticket volume will be lower during this period, and people will probably we working on training and other non-queue work. The `Eyes on FRT` role will  give us a safety-net to make sure we're getting people's attention where it's needed, as needed.  

<!-- Update current year's dates -->
- **Dates applicable**: Monday, dd December 20YY to Friday, dd January 20YY

<!-- Update current priorities -->
- **Priorities (in order)**:
   1. High Priority FRTs 
   2. Normal Priority FRTs
   3. Low Priority FRTs
   4. NRTs as guided
   
## Roles 
### 1. Eyes on FRT Tickets 

**To be very clear - this role is NOT expected to be the person DOING the tickets.  Their role is to get attention where it is needed by checking on the queue and posting an update to the team informing people where attention is needed.**

- Each day for the 2 weeks, 2 named volunteers (1 for first half of APAC, 1 for 2nd half) to check every 2 hours for what needs eyes on in the `Global FRTs` [Zendesk view](https://gitlab.zendesk.com/agent/filters/7818750574108). 
- Please coordinate with your counterpart on the day to determine what time you'll switch from the first half of APAC person to the 2nd half of APAC person
- Post a summary of needs in [#support_team-chat](https://gitlab.slack.com/archives/CCBJYEWAW) pinging `@support-team-apac`. 
    - Example summary - in order of priority need: (Note: you can do this however seems best to you, but if you’re not sure, here’s an example approach). It's useful to use a numbered list as it allows people to add number emojis to indicate which item they're going to act on.
        > - :one: _2 SM/SaaS High Priority tickets coming up to breach in the next 2 hours_
        > - :two: _1 SM/SaaS Normal Priority tickets coming up to breach in the next 2 hours_
        > - :three: _3 L&R tickets in the next 2 hours_
        > - :four: _2 SM/SaaS that have breached_ 

    - Last year @rrelax created [userscripts > queue-snapshot.js](https://gitlab.com/rrelax/userscripts/-/blob/main/zendesk/queue-snapshot.js) to generate a summary

### 2. NRT triage
- Managers will review the need for NRT responses
- They will look for high priority NRTs and NRTs that are being actively responded to for priority effort
- They’ll post an update to the 2 hourly ping thread from above drawing attention to the NRTs that need work.

### 3. Working On Tickets
- Anyone who is here, come up for air when the 2 hourly ping comes out and work tickets as needed. 
- Respond with emojis to indicate your intention.

## Coverage
1. Please volunteer for the `Eyes On FRTs` roles by updating your name below.  <br>(If this is not complete by 12 December, we will DM people to get this completed). 
    - :bangbang: Preferably don't volunteer if you're on call that day. 
1. Add/remove your name from the tables as needed. <br>The following names have been pre-filled based on the data in PagerDuty, and entered into the [2023 Holiday planning fill-out Google sheet](https://docs.google.com/spreadsheets/d/1_2gilY8PM_uYC_WF0USyTrGwW2lE5K8fafg-ueCFS8g/edit#gid=850583370) (see https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/5568+ for details). 
    - :bangbang: Please add your name for Thursday 4 and Friday 5 January 2024 if you will be present. 
1. Update `CEOC1`, `CEOC2`, `CMOC1`, `CMOC2` flag if any roster changes occur. 
1. Update `Coverage Met` tally (and status emoji :ballot_box_with_check: if met)

### Coverage Met

We will be reviewing the forecasted incoming volumes for the year end to ensure that the number of Support Engineers working each day is sufficient to have Coverage Met, indicated in the last column of the tables below. If coverage is not met, APAC Managers will be requesting team members review and modify their plans to ensure that we are able to deliver results during this period.

### Week of dd to dd December 20YY
<!-- Update current year's dates in the header of this section
and in the tables below
-->

- Statuses: 
    - :white_check_mark: Coverage met
    - :small_blue_diamond: 75% =< Coverage met
    - :exclamation: Seeking coverage 

<!-- Lookup any existing global holiday planning issues and pre-fill 
any names confirmed in the Who is here column. Lookup existing assignees 
for on-call coverage and pre-fill into the Who is here column. 

CEOC1 - https://gitlab.pagerduty.com/schedules#PQB9Q6K
CEOC2 - https://gitlab.pagerduty.com/schedules#PKPXM8K
CMOC1 - https://gitlab.pagerduty.com/schedules#PGUP5OB
CMOC2 - https://gitlab.pagerduty.com/schedules#PMPKHZN
-->

| Date        | Manager <br>(NRT triage) | Eyes on FRT <br>1st Half APAC | Eyes on FRT <br>2nd Half APAC | Who is here | Coverage Met? |
| ----------- | ------------------------ | --------------------------- | ----------------------------- | ----------- | -----------------------------|
| Monday 25    | Manager1                     | `eyes-on-APAC-1`             | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/8 :small_blue_diamond: | 
| Tuesday 26   | Manager1                 | `eyes-on-APAC-1`             | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/8 :small_blue_diamond: | 
| Wednesday 27 | Manager1                 | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/10 :exclamation: | 
| Thursday 28  | Manager1                 | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/12 :exclamation: | 
| Friday 29    | Manager1                 | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/12 :exclamation: | 

### Week of dd to dd January 20YY
| Date        | Manager <br>(NRT triage) | Eyes on FRTs<br>1st Half APAC | Eyes on FRTs<br>2nd Half APAC | Who is here | Coverage Met? |
| ----------- | ------------------------ | ----------------------------- | ----------------------------- | ----------- |  --------------------------- |
| Monday 1    | Manager2                      | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/8 :small_blue_diamond: |
| Tuesday 2   | Manager2                      | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/10 :exclamation: |
| Wednesday 3 | Manager2                      | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/12 :exclamation: |
| Thursday 4  | Manager2                      | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/12 :exclamation: |
| Friday 5    | Manager2                      | `eyes-on-APAC-1`              | `eyes-on-APAC-2`              | TeamMember1 (CEOC2), TeamMember2 (CMOC2), TeamMember3 (CMOC1), TeamMember4 (CEOC1), TeamMember5, TeamMember6 | 6/12 :exclamation: |

----

### On-call coverage request
<!-- This section not required.  -->

If you are looking for on-call coverage for the dates on this issue, feel free to add your request here for visibility. Please note, this will not be actively monitored/updated, so please ensure you keep your request up to date. 

- Statuses: 
    - :heavy_minus_sign: Pending 
    - :ballot_box_with_check: Complete 

#### Pending :heavy_minus_sign:

##### CMOC1

###### Request: 26 December 2023 to 1 January 2024
<!-- The following format is not required. Leaving it in the 
template for future use. 
-->

<details><summary>Click to expand request details</summary>

<table>
<thead>
<tr><th>Date</th><th>Who's covering</th><th>Coverage updated :ballot_box_with_check:</th></tr>
</thead>
<tbody>
<tr>
<td>Tuesday 26</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Wednesday 27</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Thursday 28/td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Friday 29</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Saturday 30</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Sunday 31</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
<tr>
<td>Monday 01</td>
<td>`@name`</td>
<td>

- [ ] Pagerduty updated
- [ ] `Who is here` updated

</td>
</tr>
</tbody>
</table>

</details>

ATT: `@gitlab-com/support/apac`

## Action Plan

1. [ ] [Create an item in the SWIR](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/) for awareness
    - [ ] Upon creation of this issue
    - [ ] 7 days prior to due date of this issue IF still seeking coverage
1. [ ] Post a message in the [`#support_team-chat`](https://gitlab.slack.com/archives/CCBJYEWAW)
   Slack channel (or other support channel as appropriate) announcing the issue
   and pointing to the SWIR announcement on `date`
1. [ ] Announce the issue and request input at the APAC Support Team Call
    - [ ] Upon creation of this issue
    - [ ] 7 days prior to due date of this issue IF still seeking coverage
1. [ ] Other communications channels
   - [ ] Discuss in 1-1s

/assign me
/label ~"Manager Attention"
/label ~"FY26 OKR::Not Applicable" 
