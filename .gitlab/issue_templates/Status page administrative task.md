<!--
Use this template when you need to change something on status.io (status.gitlab.com). For example, if you need to update or create a new component.
-->

# Summary

<!--
Please describe the change that you need to make to the status page
-->

# Request Type

- [ ] Add New component
- [ ] Update component
- [ ] Other:

# Actions

Before adding or changing components, please ensure to get a review from infrastructure counterparts from the appropriate [Reliability Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#reliability-teams).

## Adding components

If adding any new components, please ensure the following steps are done:

- [ ] On Status.io page `GitLab system status`, click on `Infrastructure` > `Add component`
- [ ] Fill in the name and select the correct containers
- [ ] Add component
- [ ] Drag the component to the appropriate spot on the list (group similar components)
- [ ] Validate the change on status.gitlab.com

## Modifying components

If updating adding any current components, please ensure the following steps are done:

- [ ] On Status.io page `GitLab system status`, click on `Infrastructure` > `Modify` for the selected component
- [ ] Make your adjustments and `Save Component`
- [ ] Validate the change on status.gitlab.com

/cc @cleveland @kenneth @faleksic @tristan

/label ~"FY26 OKR::Not Applicable" 
