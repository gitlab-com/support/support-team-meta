**Title: Support Shadow Program - Your Name**

This issue is to help organize and track progress during your participation in the GitLab Support Shadow Program.

The Support Shadow Program's first iteration is "Support for a half-day".

Team members outside the Support team volunteer to spend half of one workday shadowing and working with Support team members.

1. 60-minute prep work in advance of the Shadow (at your leisure).
1. Three 50-minute Zoom Support "Shadow" sessions
1. Short survey sharing feedback on your experience at the end.

## Stage 1: Communication and Planning

### For Support Shadow (Participant)

- [ ] Discuss your participation in the Support Shadow Program with your manager and ping them on this issue.
- [ ] Pick a week when you'll be available for calls shadowing and working alongside Support Engineers.
- Announce that you'll be participating *at least one week in advance* of your start date with the dates and your timezone, in:
    - [ ] [`#support_team-chat` Slack channel](https://gitlab.slack.com/archives/CCBJYEWAW)
    - [ ] [Support Week in Review](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/).
- [ ] Announce that you'll be participating in Support Shadow Program in #support_team-chat Slack channel one week prior to the Shadow Program.
- [ ] Schedule three Support Shadow pairing sessions during your chosen week using the following link: https://calendly.com/gitlab-support/support-shadowing

If you're unable to get 3x shadow pairing sessions scheduled during your chosen week, reach out to one of the Support Shadow Program facilitators for your region.

| Region | Facilitator(s)  |
| ------ | --------------- |
| AMER   | @gerardo        |
| EMEA   | @rmongare       |
| APAC   | @astrachan      |

## Stage 2: Before you start

### Get to know the GitLab Support team

#### Who we are

The GitLab Support team is a large globally distributed team of helpful, smart, and kind individuals.

For an overview of the Support team members you'll be working with during the Shadow Program and beyond, check the ["Meet Our Team" page for Support](https://about.gitlab.com/company/team/?department=customer-support).

#### Zendesk

We use Zendesk as our support ticketing system. If you want to be able to see tickets past and present, please:

1. [ ] [Get a light agent Zendesk account](https://handbook.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account) if you don't already have one.

#### Slack

Join the following Slack channels:

- [`#support_self-managed`](https://gitlab.slack.com/archives/C4Y5DRKLK)
- [`#support_gitlab-com`](https://gitlab.slack.com/archives/C4XFU81LG)
- [`#support_ticket-attention-requests`](https://gitlab.slack.com/archives/CBVAE1L48)
- [`#support_team-chat`](https://gitlab.slack.com/archives/CCBJYEWAW)

### Support Handbook, Workflows, and Training

Support Shadow Participants should do up to 1-hour of "prep work" of reading the GitLab Support handbook, workflows, job duties, roles, and training materials.

Familiarizing yourself with this content will provide context that can improve the Support Shadow Program experience. You are *not* expected to read or remember everything.

For the best experience, we suggest Support Shadow Program participants review the following before Shadowing:

#### Support Handbook & Workflows

- [ ] [Support Page](https://about.gitlab.com/support/)
- [ ] [Support Handbook](https://handbook.gitlab.com/handbook/support/)
- [ ] [Support Workflows](https://handbook.gitlab.com/handbook/support/workflows/)

#### Support Training

- [ ] [Support Training Handbook](https://handbook.gitlab.com/handbook/support/training/)
- [ ] [Onboarding](https://handbook.gitlab.com/handbook/support/training/#support-engineer-onboarding-pathway)
- [ ] [Training Issues and Modules](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates)
- [ ] [Git & GitLab Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Git%20and%20GitLab%20Basics.md)
- [ ] [GitLab Installation & Administration Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Gitlab%20Installation%20and%20Administration%20Basics.md)
- [ ] [Gitlab Support Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab%20Support%20Basics.md)
- [ ] [Working on Tickets](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Working%20On%20Tickets.md)
- [ ] [GitLab.com Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/GitLab-com%20SaaS%20Basics.md)
- [ ] [Self-managed Basics](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Self-Managed%20Basics.md)
- [ ] [License and Renewals](https://gitlab.com/gitlab-com/support/support-training/-/blob/master/.gitlab/issue_templates/Subscriptions%20License%20and%20Renewals.md)

## Stage 3: Shadowing and working alongside Support

Support Shadows are expected to participate in three 50-minute calls shadowing Support team members.

- [ ] Shadow 1 - Self-Managed
- [ ] Shadow 2 - SaaS
- [ ] Shadow 3 - Additional Pairing (your choice: SaaS, Self-managed, L&R, or Manager)

## Stage 4: Share Feedback and Suggestions to help improve Future Iterations

- [ ] Fill out the following survey in the issue comments to provide feedback on your experience: what was valuable, and what could be improved?

```markdown
Support Shadow Program Participant Survey

1. What interested you in the Support Shadow Program?
1. Would you recommend that others in your role participate in the Support Shadow Program?
1. On a scale of 1-10, how would you rate your Support Shadow Program Experience?
1. What did you most helpful or valuable about the Support Shadow Program?
1. What did you find least helpful or valuable during your Support Shadow Program?
1. Do you have suggestions or ideas on how this program could be improved in future iterations?
```

/label ~"FY26 OKR::Not Applicable" ~"Support Shadow Program"
