<!--
Title: Support Response: Incident-xxxx | One line description of incident
Possible alternative: Support Response: SIRT-xxx
-->

<!--
About the use of this template: 
Use this template when there has been an incident that resulted in unexpected customer impact which Support has no relevant documented/prepared response for. The purpose of this template is to coordinate Support action. It should provide all the context and info needed for a support engineer, without any prior knowledge of the situation, to quickly onboard and be able to contribute.

Support Readiness vs Support Response:
- Support Readiness is coordinating and documenting proactive actions for planned or expected customer impact; effort usually has specific DRI/s and is focused on communicating for awareness and enablement.
- Support Response is unplanned and reactive to an event with unexpected customer impact, where the response is (usually) coordinated by CMOCs, but with ongoing collaboration from anyone in Support and internal teams. The response is dynamic and influenced by the related incident.
-->


## Situation Overview

- Issue link:


### What do we know?


## Support Response

### What can we say to users?
<!-- Note specific guidelines on communication with customers, especially relating to confidential vs public information -->



### What can we do to help users?
<!-- Provide step-by-step instructions -->



## Checklist for Response Actions

<details><summary>Click to expand</summary>

1. [ ] Provide user communication guidelines (what can we say section)
1. [ ] Outline the actions to take (what can we do section)
1. [ ] Create incident tag in ZD 
1. [ ] Document ZD ticket response
1. [ ] Communicate response plan to Support team
1. [ ] Assign issue to DRIs and CMOCs 

</details>

## Additional Resources & Impact Tracking

<details><summary>Related links</summary>
<!-- [~] Disabled means not relevant. [x] Checked if there is a link to provide. -->

- [~] Incident Slack channel:
- [~] Related Slack threads: 
- [~] Gdoc link for sync meeting notes:
- [~] Blog post: 
- [~] [Marketing comms]((https://handbook.gitlab.com/handbook/support/workflows/sending_notices/#mass-emails-through-marketing-department)) plan: 

</details>

<details><summary>Related ZD tickets</summary>

- ZenDesk search on incident tag link:

Ticket examples:

- Ticket 1
- Ticket 2

</details>

/label ~"Support Response"
/assign me
/confidential
<!-- 
Keep confidential: this issue is for Support and internal team coordination during an incident and shouldn't be used as a resource for public consumption; public can be directed to the incident issue and status page.
-->