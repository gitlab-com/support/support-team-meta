# Summary

This checklist is a supplementary tool to help quickly coordinate tasks during an incident where a feature-related issue impacts many customers, results in multiple customer emergencies, and requires collaboration across teams to resolve.

These tasks and workflows relating to the roles are documented in the handbook on call guide: https://handbook.gitlab.com/handbook/support/on-call/#on-call-in-gitlab-support. The handbook is the source of truth for these processes. This checklist is intended to supplement, not replace, the handbook guidance.

### Feature-related Incident Checklist

#### Roles and Responsibilities specific to this type of incident:

- CMOC (Communications Manager On-Call): Coordinates customer communication and overall incident response between relevant teams
- CEOC (Customer Emergencies On-Call): Takes lead in coordinating emergency ticket response and resolution 
- SMOC (Support Manager On-Call): Manages support team resources and cross-team coordination during incident

## CMOC Tasks

- [ ] Create this incident checklist and assign yourself, the CEOC, and the SMOC.
- [ ] Follow [CMOC workflow](https://handbook.gitlab.com/handbook/support/workflows/cmoc_workflows) | [Optional template for CMOC workflow](https://gitlab.com/gitlab-com/support/emergency-runbook/blob/master/.gitlab/issue_templates/Support%20-%20CMOC.md)

#### Visibility for relevant teams
- [ ] Create a support readiness issue or update existing issue with details - this is now the SSOT
- [ ] Assign readiness issue to yourself, the CEOC, and the SMOC
- [ ] Tag Product manager and Engineering manager to alert them of the issue and impact
- [ ] Double check all readiness issue details and confirm confidentiality requirements
- [ ] Keep issue updated with status and timeline of relevant events

#### Confidentiality

- [ ] Communication to customers (like information in the macro, readiness issue, and ticket responses) should be aligned with confidentiality of the incident and existing documentation or comms around the product. When confidentiality is in doubt or changed, consult with the PM/comms team.

## CEOC Tasks

#### Handling [customer emergencies](https://handbook.gitlab.com/handbook/support/workflows/customer_emergencies_workflows/#determine-if-the-situation-qualifies-as-an-emergency)
- [ ] Follow the workflows: 
   - [ ] [A widespread incident causes multiple, successive PagerDuty alerts](https://handbook.gitlab.com/handbook/support/workflows/customer_emergencies_workflows/#a-widespread-incident-causes-multiple-successive-pagerduty-alerts)
   - [ ] [Handling multiple emergencies](https://handbook.gitlab.com/handbook/support/workflows/customer_emergencies_workflows/#handling-multiple-simultaneous-emergencies)

#### Workflows for tasks to perform to address/investigate/resolve the issue
   - [ ] Document set of tasks as a workflow in the support readiness issue

#### Support tooling/solutions
   - [ ] If a Support tool or workflow is built which requires or uses elevated permissions (example: a script that runs with Admin made available to use for engineers without Admin), consult with the Security team.

#### Emergency ticket collaboration
   - [ ] Maintain separate threads in Slack to discuss specific tickets and keep thread focused on that specific ticket being discussed.

## Support Manager On-call Tasks

- [ ] Follow [SMOC workflow for handling customer emergencies](https://handbook.gitlab.com/handbook/support/workflows/support_manager-on-call/#handling-customer-emergencies-and-incidents)

#### Coordination across timezones
- [ ] Create Slack thread to: point to readiness issue as SSOT, coordinate work/tasks among SEs, ask questions about issue in general
- [ ] Create end-of-day update in Slack for next timezone SMOC

#### Coordinate additional resources
- [ ] Coordinate with CMOC and CEOC to identify and allocate additional support engineers if needed
- [ ] Reach out or help escalate to other teams (e.g., Product, Engineering) for specialized assistance if required
- [ ] Ensure proper handoff of information and tasks for the next timezone and on-call team