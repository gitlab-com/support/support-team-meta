## Intent

This issue describes how we are [facilitating a Support Manager to take meaningful time away from work](https://handbook.gitlab.com/handbook/support/support-time-off/). The intent is that all important tasks/responsibilities are delegated, reassigned, or intentionally put on hold, and the manager isn't required to "catch up" before and after taking a well-deserved vacation.

This Issue Template is **required** for PTO lasting longer than 1 week. It is optional for PTO lasting 1 week or less.

## :handshake: Responsibilities and :muscle: Coverage Tasks

Can include communications with direct reports; work on active OKRs; in-flight support-team-meta Issues; in-flight Account Escalations or Escalated Customers. See the [Support Manager Responsibilities handbook page](https://handbook.gitlab.com/handbook/support/managers/manager-responsibilities/) for other possible items.

| Priority | Theme | Context Link | Primary | Secondary |
| -------- | ----- | ------------ | ------- | --------- |
| HIGH/MEDIUM/LOW | On-call rotation | [link to PD schedule]() | person | person |
| HIGH/MEDIUM/LOW | DRI for Account Escalation | [link to Escalation doc/channel]() | person | N/A |

Include specific issue links for tasks to be completed or followed up on.

- [ ] Item - Link - `@assignee`

## :white_check_mark: Issue Tasks

### :o: Tasks before taking PTO

- [ ] Instantiate this Issue Template
  - [ ] Assign to yourself
  - [ ] Title the issue `Support Manager Coverage for YOUR-NAME from XXXX-XX-XX until XXXX-XX-XX`
  - [ ] Set the due date as your last PTO day
  - [ ] Add and assign all items that need substitutes under the Responsibilities & Coverage section
  - [ ] Add any relevant References including direction pages, group handbook pages, etc
  - [ ] Assign this Issue to anyone who has a specific task or responsibility assigned to them
- [ ] Review the [guidelines to communicate your time off](https://about.gitlab.com/handbook/paid-time-off/#communicating-your-time-off)
- [ ] Assign coverage to your substitute in PTO by Deel.
- [ ] Ensure your PTO by Deel auto-responder points team members to this issue.
- [ ] Share this issue in the `#spt_managers-emea` Slack channel and with your direct reports.
- [ ] (optional) Update your [GitLab status](https://docs.gitlab.com/ee/user/profile/#set-your-current-status) to include a reference to this issue.
- [ ] (optional) Update your Zendesk status in the [Zendesk Out Of Office App](https://handbook.gitlab.com/handbook/support/support-time-off/#preparing-for-time-off).
- [ ] (optional) Consider creating autoresponders for [Gmail](https://support.google.com/mail/answer/25922), [LinkedIn Recruiter](https://www.linkedin.com/help/linkedin/answer/a550614/set-and-edit-away-message-on-linkedin), and other tools you use day to day.
- [ ] (optional) Setup Slack apps for your time off, like [Donut](https://about.gitlab.com/company/culture/all-remote/tips/#coffee-chats), [Geekbot](https://help.geekbot.com/en/articles/4311732-how-can-i-schedule-an-out-of-office-period), etc.

### :x: Tasks after coming back from PTO

- [ ] Review the [Returning from Time Off](https://handbook.gitlab.com/handbook/support/support-time-off/#catching-up-after-a-scheduled-time-off) guidance.
- [ ] Assign this Issue back to yourself and remove others.
- [ ] Catch up with the other managers and follow up on details your substitute wrote up
- [ ] Catch up on Slack
- [ ] Read through the [SWIR](https://docs.google.com/document/d/1eyMzbzImSKNFMpmu33C6imvC1iWEWHREJqaD6mkVDNg/edit#heading=h.sn5ptqnpxw4v)
- [ ] Follow up on [EMEA Support Manager meeting](https://docs.google.com/document/d/1hDGaHOySHxQGDkxYsxFQRTuvO39NexWMOSdtxhHosPk/edit#heading=h.cfobl2csqg8j)
- [ ] Follow up on [Support Team call](https://docs.google.com/document/d/1oyi9BtoaNwZE99KNHC-9HiYX7g2bCLu986kjzJL9dO4/edit#heading=h.uuufjngstxd)  

/label ~"FY26 OKR::Not Applicable"
