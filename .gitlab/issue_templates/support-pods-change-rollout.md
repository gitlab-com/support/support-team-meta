# GitLab Support Pods: Change Rollout Plan
# `Process Change Title`

## The Story

*Write here the words you want Support Pod leads to use in explaining the change fully - what's
changing, how it's changing, why it's changing. This is the best opportunity to ensure consistency
across Support Pods.*

## Expectations

*Describe what is required of Support Pod leads when they review this issue.*

* Example: Your input is required before this change goes into effect. Check off your name when you
  have analyzed and commented as appropriate.
* Example: Your acknowledgement is required for this change that takes effect on date XYZ. Check
  off your name to indicated you have reviewed the information, and will share it with your team.

## Schedule

* Rollout to begin on `this date`
* Will the rollout be phased, such as by team or region? If so, describe here the schedule and order.
* Adoption complete by `this date`

## Due Date

Check off your name by midnight UTC on: `202x-xx-xx`

## Support Pod Leads

*Remove any subsections that are not applicable to your issue.*

*Known issue: How to handle de-duplication of names. Sorry, folks who have to check multiple boxes!*

Sorted alphabetically by Support Pod and GitLab handle.

AI

* [ ] @jgaughan

Authentication and Authorization

* [ ] @alejguer
* [ ] @asmaa.hassan
* [ ] @gerardo
* [ ] @j.castillo

CI/CD

* [ ] @manuelgrabowski 

Code Contributions

* [ ] @anton
* [ ] @manuelgrabowski

Database

* [ ] @bprescott_

Geo

* [ ] @anton
* [ ] @klang
* [ ] @rvzon

GET

* [ ] @lwbrown

Git and Gitaly

* [ ] @jessie

GitLab Dedicated

* [ ] @ahergenhan
* [ ] @bcarranza
* [ ] @dkua1
* [ ] @weimeng-gtlb
* [ ] @wwjenkins

Import and Integrate

* [ ] @anton

Kubernetes

* [ ] @lwbrown

Performance and Stability

* [ ] @cody

Runner

* [ ] @jfarmiloe
* [ ] @tmarsh1

Secure

* [ ] @bcarranza
* [ ] @katrinleinweber

Training

* [ ] @jgaughan
* [ ] @mbadeau

Upgrade

* [ ] ???

## Follow-Up Plan

*How will you follow-up to understand the results of the change, to make adjustments appropriately,
and to rollback if necessary? These are typical questions you might want to answer here:*

> * How will results be captured? By whom and by when?
> * What is the plan for considering and making quick improvements?
> * What is the plan should the change be deemed unsuccessful?
>   * Is a rollback feasible, and if so how will it happen?

## Rollout Checklist

1. [ ] [Create an item in the SWIR](https://gitlab-com.gitlab.io/support/toolbox/forms_processor/SWIR/)
   to announce the change and include [The Story](#the-story) on `date`
1. [ ] Post a message in the [`#support_team-chat`](https://gitlab.slack.com/archives/CCBJYEWAW)
   Slack channel (or other support channel as appropriate) announcing the change and pointing to
   the SWIR announcement on `date`
1. [ ] Other communications channels
   - [ ] Other communications channels, if required - for example, post to a CSM Slack channel if
     the CSMs are "impacted non-users"
1. [ ] Leave feedback on change adoption, concerns, etc. in this issue by `date`

/assign me
/label ~support-change-rollout
/label ~"FY26 OKR::Not Applicable" 
